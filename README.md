# Boilerplate ExpressJS

To read:
- https://github.com/airbnb/javascript#readme
- https://github.com/remy/nodemon#readme

Modules to extract as npm package:
- Request body, get validation
- Resources access permissions
- MongoDB connection + Model
- Logs